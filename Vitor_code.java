package Parameters;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GS {

	// number of partners
	private int N;

	// from 0 to N, counter of engaged
	private int partners;

	// woman relationships
	private String[] wPartner;

	// status of men, engaged or not
	private boolean[] menStatus;

	// men
	private String[] m;

	// women
	private String[] w;

	// men's preference
	private String[][] mp;

	// women's preference
	private String[][] wp;

	// men names (if input is a string name)
	private String[] mNames;

	// women names (if input is a string name)
	private String[] wNames;

	// main
	public static void main(String[] args) throws FileNotFoundException {

		File file = new File(args[0]);
		Scanner scan = new Scanner(file);

		GS gs = new GS(scan);
	}

	public GS(Scanner scan) {

		GetDataFromFile(scan);
		GsAlgorithm();
		ShowResults();
	}

	private void GetDataFromFile(Scanner scan) {

		int pcount = 1;
		int ppreference = 1;
		int wcount = 0;
		int mcount = 0;
		int wpcount = 0;
		int mpcount = 0;
		String availalbe;

		while (scan.hasNextLine()) {
			availalbe = scan.nextLine();
			String[] items = availalbe.split(" ");

			// get N
			if (items[0].startsWith("n=")) {
				String elem = items[0].substring(2);
				N = Integer.parseInt(elem);
				createArrayInstances(N);
			}

			// get people
			if (isNumeric(items[0])) {
				String[] elem = availalbe.split(" ");
				if (pcount % 2 == 0) {
					String personNumber = ifStringGetIndex(elem[1], pcount,
							wcount, wNames);
					w[wcount] = personNumber;
					wcount++;
				} else {
					String personNumber = ifStringGetIndex(elem[1], pcount,
							mcount, mNames);
					m[mcount] = personNumber;
					mcount++;
				}
				pcount += 1;
			}

			// get preferences
			Pattern pattern = Pattern.compile("\\d+:");
			Matcher matcher = pattern.matcher(items[0]);
			if (matcher.matches()) {
				String[] elem = availalbe.split(" ", 2);

				String[] prefer = elem[1].split(" ");

				if (ppreference % 2 == 0) {
					for (int i = 0; i < prefer.length; i++) {
						wp[wpcount][i] = prefer[i];
					}
					wpcount++;
				} else {
					for (int i = 0; i < prefer.length; i++) {
						mp[mpcount][i] = prefer[i];
					}
					mpcount++;
				}
				ppreference += 1;
			}
		}
		scan.close();
	}

	private static String ifStringGetIndex(String string, int pcount,
			int nameCounter, String[] names) {
		if (isNumeric(string))
			return string;
		else {
			names[nameCounter] = string;
			return Integer.toString(pcount);
		}
	}

	private void createArrayInstances(int n) {

		m = new String[n];
		w = new String[n];
		mp = new String[n][n];
		wp = new String[n][n];
		mNames = new String[n];
		wNames = new String[n];
		menStatus = new boolean[n];
		wPartner = new String[n];
	}

	public static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	// GS algorithm
	private void GsAlgorithm() {

		while (partners < N) {
			int count;
			for (count = 0; count < N; count++)
				if (menStatus[count] == false)
					break;

			for (int i = 0; i < N && menStatus[count] == false; i++) {
				
				//get index of men's preference
				int index = getIndexOf(mp[count][i], w);

				if (wPartner[index] == null) {
					//set this man to the women engaged status 
					wPartner[index] = m[count];
					menStatus[count] = true;
					partners += 1;
				} else {
					// get actual partner
					String currentPartner = wPartner[index];

					//compare actual with new partner
					if (wMorePreference(currentPartner, m[count], index)) {
						//update the women engaged status
						wPartner[index] = m[count];
						menStatus[count] = true;
						menStatus[getIndexOf(currentPartner, m)] = false;
					}
				}
			}
		}
	}

	// check if woman prefer other better partner
	private boolean wMorePreference(String actualPartner, String newPartner,
			int index) {

		for (int i = 0; i < N; i++) {
			if (wp[index][i].equals(newPartner))
				return true;
			if (wp[index][i].equals(actualPartner))
				return false;
		}

		return false;
	}

	// get the index of m or p, in its array
	private int getIndexOf(String str, String[] list) {
		for (int i = 0; i < N; i++)
			if (list[i].equals(str))
				return i;
		return -1;
	}

	// show results
	public void ShowResults() {
		// System.out.println("Couples are : ");
		for (int i = 0; i < N; i++) {
			if (mNames[0] != null) {
				System.out
						.println(mNames[(Integer.parseInt(wPartner[i]) - 1) / 2]
								+ " -- "
								+ wNames[(Integer.parseInt(w[i]) - 1) / 2]);
			} else
				System.out.println(wPartner[i] + " -- " + w[i]);
		}
	}
}
