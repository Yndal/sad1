import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by Yndal on 28-08-2014.
 */
public class GS {

    Person[] persons;
    Stack<Man> freeMen;
    String filename = "stable-marriage-worst-500";

    public static void main(String[] args){
        GS exercise = new GS();
        exercise.run();
    }

    public void run(){
    	long start = new Date().getTime();
        loadData();
        System.out.println("Done loading: " + ((long) new Date().getTime() - start)/1000);
        solve();
        printRelationShips();
        long end = new Date().getTime();
        long diff = end - start;
        System.out.println("Time: " + (long) diff/1000 + " seconds");
        
      //  compareData();
    }


    private void loadData() {
        try{
            Scanner scanner = new Scanner(new BufferedInputStream(System.in));//new FileInputStream("D:\\Workspace\\Java\\TempStableMarriage\\data-in\\" + filename + ".in")));//System.in));
            String firstLine = scanner.nextLine();
            boolean ready = false;
            do{
                if(!firstLine.startsWith("#"))
                    ready = true;
                else
                    firstLine = scanner.nextLine();
            } while(!ready);

            //Create data structure
            int n = Integer.valueOf(firstLine.substring(firstLine.indexOf("=")+1));

            persons = new Person[n*2+1];//Wasting 1 spot in the very front of the array to keep person ID similar with the index

            //Load persons
            String line = scanner.nextLine();
            boolean done = false;
            while(!done){

                int spaceIndex = line.indexOf(" ");
                int id = Integer.valueOf(line.substring(0,spaceIndex));
                String name = line.substring(spaceIndex+1);

                //debug(id + ":" + name);

                if(id % 2 == 1)
                    persons[id] = new Man(id, name);
                else
                    persons[id] = new Woman(id, name);
                line = scanner.nextLine();
                if(line.equals(""))
                    done = true;
            }

            //Load preferences
            line = scanner.nextLine();
            while(!line.equals("")){
                //Get person id
                int index = line.indexOf(":");
                int personId = Integer.valueOf(line.substring(0,index));

                //Get preferences
                ArrayList<Integer> prefs = new ArrayList<Integer>(n);
                line = line.substring(index+1);

                while(line.indexOf(" ") != -1){
                    index = line.indexOf(" ");
                    int indexLast = line.substring(index+1).indexOf(" ");
                    if(indexLast < 0)
                        indexLast = line.length()-1;

                    int pref = Integer.valueOf(line.substring(index+1, indexLast+1));
                    prefs.add(pref);

                    line = line.substring(indexLast+1);
                }


                //Set preferences
                if(personId%2 == 1){
                    //Person is a man
                    Stack<Integer> p = new Stack<Integer>();
                    for(int i=prefs.size()-1; 0<=i; i--)
                        p.push(prefs.get(i));

                    ((Man)persons[personId]).setPreferences(p);
                } else {
                    //Person is a woman
                    ((Woman) persons[personId]).setPreferences(prefs);
                }

                //Are we at the end?
                line = scanner.hasNextLine() ? scanner.nextLine() : "";
            }
            scanner.close();
        } catch (IllegalArgumentException iae){
            System.out.println("Unable to read file: " + iae.getMessage());
        }
    }


    public void solve(){
        freeMen = new Stack<Man>();

        for(int i=0; i<persons.length; i++)
            if(i%2==1)
                freeMen.push((Man)persons[i]);

        //While any free man -> let him propose
        while(!freeMen.isEmpty()){
            Man m = freeMen.lastElement();
            int nextWoman = m.nextPreferredWoman();

            //The chosen man will propose to his preferred women
            while(!m.proposeTo((Woman) persons[nextWoman]))
                nextWoman = m.nextPreferredWoman();
        }
    }

    private void compareData(){
    	int lCounter = 1;
    	int fails = 0;
    	try {
    		Scanner scanner = new Scanner(new BufferedInputStream(/*System.in));/*/new FileInputStream("D:\\Workspace\\Java\\TempStableMarriage\\data-in\\" + filename + ".out")));
    		while(scanner.hasNext()){
    			String pLine = persons[lCounter].getName() + " -- " + persons[lCounter].engagedTo.getName();
    			//pLine = pLine.trim();

    			lCounter += 2;
    			String scannerLine = scanner.nextLine();//.trim();
    			if(!scannerLine.equals(pLine)){
    				fails++;
    			}
    		}
    		
    		System.out.println("Total mismatches: " + fails);
    	 
    	} catch (FileNotFoundException e) {
    		System.out.println("Unable to read pre-generated .out file");
			e.printStackTrace();
		}
         
    }

    private void debug(Object o){
        System.out.println(o.toString());
    }


    private void printRelationShips(){
        for(int i=1; i<persons.length; i++){
            if(persons[i].getId()%2==1){
                System.out.println(persons[i].getName() + " -- " + persons[i].engagedTo.getName());
            }
        }
    }



    private class Person{
        private int id;
        private String name;
        protected Person engagedTo = null; //If this is null, this person is still free

        public Person(int id, String name){
            this.id = id;
            this.name = name;
        }

        public int getId(){
            return id;
        }

        public String getName(){
            return name;
        }

   /*     public boolean isEngaged(){
        	return engagedTo != null;
        }*/
    }

    private class Man extends Person{
        private Stack<Integer> preferences;

        public Man(int id, String name){
            super(id, name);
        }

        public void setPreferences(Stack<Integer> prefs){
            if(preferences == null)
                preferences = prefs;
        }
        
    /*    public Stack<Integer> getPreferences(){
        	return preferences;
        }*/

        public int nextPreferredWoman(){
            return preferences.pop();
        }

        public boolean proposeTo(Woman w){
            if(w.willAcceptProposalFrom(this)){
                this.engagedTo = w;
                freeMen.remove(this);

                return true;
            }

            return false;

        }

    }


    private class Woman extends Person{
        List<Integer> preferences;

        public Woman(int id, String name){
            super(id, name);
        }

        public void setPreferences(List<Integer> prefs){
            if(preferences == null)
                preferences = prefs;
        }

        public boolean willAcceptProposalFrom(Man m){
            if(engagedTo == null){
                engagedTo = m;
                return true;
            }
            if(wantsNewMan(m)){
                divorceFrom((Man) this.engagedTo);
                engagedTo = m;
                return true;
            }

            return false;
        }

        private boolean wantsNewMan(Man m){
            if(this.engagedTo == null)
                return true;

            return preferences.indexOf(m.getId()) < preferences.indexOf(engagedTo.getId());
        }

        private void divorceFrom(Man m){
            m.engagedTo = null;
            freeMen.push(m);
        }
    }
}
